import { Component, OnInit } from '@angular/core';
import { AdminState } from '../../../shared/state/admin.state';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  public components = [
    {
      name: 'Dashboard',
      icon: 'event',
      url: 'dashboard',
    },
    {
      name: 'API',
      icon: 'event',
      url: 'api',
    },
    {
      name: 'Calendar',
      icon: 'event',
      url: 'calendar',
    },
    {
      name: 'Batch',
      icon: 'event',
      url: '/admin/api/batch',
    },
  ];

  constructor(public state: AdminState) {}

  ngOnInit(): void {}
}
