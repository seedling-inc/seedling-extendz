package com.hrandika.seedling.spring.modules.product

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.security.Principal
import java.util.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping(value = ["\${spring.data.rest.base-path}/products"])
class ProductController(val productService: ProductService) {

    @PostMapping(value = ["/{id}/image"])
    fun setProductImage(
        @PathVariable(value = "id", required = true) id: String,
        @RequestParam(value = "image", required = true) file: MultipartFile,
        principal: Principal?,
        request: HttpServletRequest?
    ): ResponseEntity<*>? {
        val p: Optional<Product> = productService.uploadImages(file, id)
        return ResponseEntity.of<Product>(p)
    }
}