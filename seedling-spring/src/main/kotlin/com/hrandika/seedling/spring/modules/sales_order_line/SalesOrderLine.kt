package com.hrandika.seedling.spring.modules.sales_order_line

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.core.common.price.Price
import com.hrandika.seedling.spring.modules.product.Product
import com.hrandika.seedling.spring.modules.sales_order.SalesOrder
import org.springframework.data.repository.PagingAndSortingRepository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "sales_order_line", uniqueConstraints = [UniqueConstraint(columnNames = ["line_no", "sales_order_id"])])
data class SalesOrderLine(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @Column(name = "line_no", nullable = false)
    var lineNo: Long? = null,

    @Column(nullable = false)
    var quantity: Long? = null,

    @Column(name = "delivered_quantity", nullable = false)
    var deliveredQuantity: Long? = null,

    @Embedded
    var price: Price? = null,

    @Column(name = "discount_type")
    val discountType: DiscountType? = null,

//    @Embedded
//    @AttributeOverrides(
//        AttributeOverride(name = "value", column = Column(name = "discount_value")),
//        AttributeOverride(name = "currency", column = Column(name = "discount_currency"))
//    )
//    var discount: Price? = null,

    @Column(name = "order_line_status", nullable = false)
    val orderLineStatus: OrderLineStatus? = null,

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    var product: Product? = null,

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    var salesOrder: SalesOrder? = null

) : BaseEntity()

enum class DiscountType {
    ByPrice,
    ByPercentage
}

enum class OrderLineStatus {
    Planned,
    Released,
    Reserved,
    PartiallyDelivered,
    Delivered
}

interface SalesOrderLineRepository : PagingAndSortingRepository<SalesOrderLine, Long>
