package com.hrandika.seedling.spring.modules.sales_order_line

import com.hrandika.seedling.spring.modules.product.ProductNameProjection
import org.springframework.data.rest.core.config.Projection

@Projection(name = "product", types = [SalesOrderLine::class])
interface SalesOrderLineProductProjection {
    fun getProduct(): ProductNameProjection
    fun getQuantity(): Int
}