package com.hrandika.seedling.spring.config


import com.hrandika.seedling.spring.modules.currency.Currency
import org.springframework.context.annotation.Configuration
import org.springframework.data.rest.core.config.RepositoryRestConfiguration
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer
import org.springframework.web.servlet.config.annotation.CorsRegistry

/***
 * @author Randika Hapugoda
 */
@Configuration
class GlobalRepositoryRestConfigurer() : RepositoryRestConfigurer {

    override fun configureRepositoryRestConfiguration(config: RepositoryRestConfiguration?, cors: CorsRegistry) {
        config?.exposeIdsFor(
            Currency::class.java
        )
        cors.addMapping("/**")
            .allowedOrigins("*")
            .allowedMethods("*")
            .allowedHeaders("*")
    }


}
