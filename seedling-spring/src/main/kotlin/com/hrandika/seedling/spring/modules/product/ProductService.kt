package com.hrandika.seedling.spring.modules.product

import com.hrandika.seedling.spring.core.services.s3.S3Directories
import com.hrandika.seedling.spring.core.services.s3.S3Service
import com.hrandika.seedling.spring.utils.logger
import org.slf4j.Logger
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.util.*

@Service
class ProductService (
    var productRepository: ProductRepository,
    var s3Service: S3Service ) {

    val log: Logger = logger<ProductService>()!!

    fun uploadImages(file: MultipartFile, id: String): Optional<Product> {
        val optional = this.productRepository.findById(id)

        if (optional.isPresent) {
            val url: String = this.s3Service.uploadImage(file, S3Directories.PRODUCTS_IMAGES)
            val entity = optional.get()
            entity.image = url
            this.productRepository.save(entity)
            return Optional.of(entity);
        }
        return Optional.empty()
    }

}