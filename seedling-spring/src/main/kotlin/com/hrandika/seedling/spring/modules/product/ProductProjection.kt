package com.hrandika.seedling.spring.modules.product

import org.springframework.data.rest.core.config.Projection

@Projection(name = "name", types = [Product::class])
interface ProductNameProjection {
    fun getName(): String
}

@Projection(name = "dataTable", types = [Product::class])
interface ProductProjection : ProductNameProjection {
}
