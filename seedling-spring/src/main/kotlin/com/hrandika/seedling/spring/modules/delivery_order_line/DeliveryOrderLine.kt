package com.hrandika.seedling.spring.modules.delivery_order_line

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.modules.delivery_order.DeliveryOrder
import com.hrandika.seedling.spring.modules.sales_order_line.SalesOrderLine
import org.springframework.data.repository.PagingAndSortingRepository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "delivery_order_line", uniqueConstraints = [UniqueConstraint(columnNames = ["line_no", "delivery_order_id"])])
data class DeliveryOrderLine(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @Column(name = "line_no", nullable = false)
    var lineNo: Long? = null,

    @Column(nullable = false)
    var quantity: Long? = null,

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    var salesOrderLine: SalesOrderLine? = null,

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    var deliveryOrder: DeliveryOrder? = null

) : BaseEntity()

interface DeliveryOrderLineRepository : PagingAndSortingRepository<DeliveryOrderLine, Long>
