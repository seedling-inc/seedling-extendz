package com.hrandika.seedling.spring.modules.customer_payment

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.core.common.price.Price
import com.hrandika.seedling.spring.modules.sales_order.SalesOrder
import org.springframework.data.repository.PagingAndSortingRepository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "customer_payment", uniqueConstraints = [UniqueConstraint(columnNames = ["line_no", "sales_order_id"])])
data class CustomerPayment(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @Column(name = "line_no", nullable = false)
    var lineNo: Long? = null,

    var description: String? = null,

    @Embedded
    @Column(nullable = false)
    var amount: Price? = null,

    var note: String? = null,

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    var salesOrder: SalesOrder? = null

) : BaseEntity()

interface CustomerPaymentRepository : PagingAndSortingRepository<CustomerPayment, Long>
