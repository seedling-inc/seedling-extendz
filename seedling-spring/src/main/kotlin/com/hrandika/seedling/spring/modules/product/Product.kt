package com.hrandika.seedling.spring.modules.product

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.core.common.price.Price
import com.hrandika.seedling.spring.core.common.unit.Unit
import com.hrandika.seedling.spring.modules.batch.Batch
import com.hrandika.seedling.spring.modules.category.Category
import com.hrandika.seedling.spring.modules.supplier_for_product.SupplierForProducts
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import org.hibernate.annotations.TypeDef
import org.hibernate.annotations.TypeDefs
import javax.persistence.*

@Entity
@Table(name = "product")
@TypeDefs(TypeDef(name = "jsonb", typeClass = JsonBinaryType::class))
data class Product(
    @Id
    var id: String? = null,

    @Column(nullable = false)
    var name: String? = null,

    var description: String? = null,

    @Column(name = "minimum_quantity")
    var minimumQuantity: Long? = null,

    @Column(name = "expirable")
    var expirable: Boolean? = null,

    @Embedded
    var price: Price? = null,

    @Embedded
    @AttributeOverrides(
        AttributeOverride(name = "value", column = Column(name = "measure_value")),
        AttributeOverride(name = "unit", column = Column(name = "measure_unit"))
    )
    var measure: Unit? = null,

    var note: String? = null,

    var image: String? = null,

    var active: Boolean? = true,

    @OneToOne(fetch = FetchType.EAGER)
    var category: Category? = null,

    @OneToMany(fetch = FetchType.LAZY)
    var batches: List<Batch>? = null,

    @OneToMany(fetch = FetchType.LAZY)
    var supplierForProducts: List<SupplierForProducts>? = null

) : BaseEntity()

@Repository
interface ProductRepository : PagingAndSortingRepository<Product, String>
