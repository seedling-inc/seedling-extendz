package com.hrandika.seedling.spring.utils

import com.hrandika.seedling.spring.modules.currency.Currency
import com.hrandika.seedling.spring.modules.currency.CurrencyRepository
import com.hrandika.seedling.spring.modules.customer.Customer
import com.hrandika.seedling.spring.modules.customer.CustomerRepository
import com.hrandika.seedling.spring.modules.product.Product
import com.hrandika.seedling.spring.modules.product.ProductRepository
import com.hrandika.seedling.spring.modules.system_user.SystemUser
import com.hrandika.seedling.spring.modules.system_user.SystemUserRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class InitData(
    var systemUserRepository: SystemUserRepository,
    var currencyRepository: CurrencyRepository,
    var customerRepository: CustomerRepository,
    var productRepository: ProductRepository,
    var passwordEncoder: PasswordEncoder
) {

    @PostConstruct
    fun init() {
        // admin user
        var admin =
            SystemUser("admin@seedling.lk", this.passwordEncoder.encode("gadgetM@N12x89"), mutableListOf("ROLE_ADMIN"))
        this.systemUserRepository.save(admin)

        // currency
        var currency = Currency("LKR", name = "Rs")
        this.currencyRepository.save(currency)

        // product
        var product = Product(name = "Test Product")
        product.id = "one"
        this.productRepository.save(product)

        // customer
        var customer = Customer()
        customer.firstName = "Randika"
        customer.lastName = "Hapugoda"
        this.customerRepository.save(customer)
    }

}