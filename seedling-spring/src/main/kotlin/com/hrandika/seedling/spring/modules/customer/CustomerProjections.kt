package com.hrandika.seedling.spring.modules.customer

import com.hrandika.seedling.spring.modules.address.AddressCityProjection
import org.springframework.data.rest.core.config.Projection

@Projection(name = "entity", types = [Customer::class])
interface CustomerEntityProjection {
    fun getFirstName(): String
    fun getLastName(): String
    fun getAddresses(): List<AddressCityProjection>
}