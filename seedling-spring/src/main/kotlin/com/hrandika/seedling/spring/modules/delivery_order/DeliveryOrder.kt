package com.hrandika.seedling.spring.modules.delivery_order

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.modules.delivery_order_line.DeliveryOrderLine
import com.hrandika.seedling.spring.modules.delivery_vehicle.DeliveryVehicle
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import org.hibernate.annotations.Type
import java.time.OffsetDateTime
import javax.persistence.*

@Entity
@Table(name = "delivery_order")
data class DeliveryOrder(
    @Id
    var id: String? = null,

    @Column(name = "dispatch_date", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    val dispatchDate: OffsetDateTime? = null,

    var note: String? = null,

    @Column(name = "delivery_status", nullable = false)
    val deliveryStatus: DeliveryStatus? = null,

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    var deliveryVehicle: DeliveryVehicle? = null,

    @OneToMany(fetch = FetchType.EAGER)
    var deliveryOrderLine: List<DeliveryOrderLine>? = null

) : BaseEntity()

enum class DeliveryStatus {
    Planned,
    OnRoute,
    Closed
}

@Repository
interface DeliveryOrderRepository : PagingAndSortingRepository<DeliveryOrder, String>
