package com.hrandika.seedling.spring.modules.sales_order

import com.hrandika.seedling.spring.modules.sales_order_line.SalesOrderLineProductProjection
import org.springframework.data.rest.core.config.Projection
import java.time.OffsetDateTime

@Projection(name = "entity", types = [SalesOrder::class])
interface SalesEntityOrderProjections {
    fun getId(): String
    fun getType(): OrderType
    fun getWantedDeliveryDate(): OffsetDateTime
    fun getOrderLines(): List<SalesOrderLineProductProjection>
}