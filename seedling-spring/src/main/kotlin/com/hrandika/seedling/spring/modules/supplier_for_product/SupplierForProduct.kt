package com.hrandika.seedling.spring.modules.supplier_for_product

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.core.common.price.Price
import com.hrandika.seedling.spring.modules.supplier.Supplier
import com.hrandika.seedling.spring.modules.product.Product
import org.springframework.data.repository.PagingAndSortingRepository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "supplier_for_products", uniqueConstraints = [UniqueConstraint(columnNames = ["supplier_id", "product_id"])])
data class SupplierForProducts(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    var id: Long? = null,

    @Embedded
    @Column(nullable = false)
    var price: Price? = null,

    var note: String? = null,


    var active: Boolean? = true,

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    var supplier: Supplier? = null,

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    var product: Product? = null

) : BaseEntity()

interface SupplierForProductsRepository : PagingAndSortingRepository<SupplierForProducts, Long>
