package com.hrandika.seedling.spring.modules.delivery_vehicle

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "delivery_vehicle")
data class DeliveryVehicle(
    @Id
    var id: String? = null,

    @Column(name = "register_no", nullable = false)
    var registerNo: String? = null,

    var description: String? = null,

    var active: Boolean? = true,

) : BaseEntity()

@Repository
interface DeliveryVehicleRepository : PagingAndSortingRepository<DeliveryVehicle, String>
