package com.hrandika.seedling.spring.modules.supplier

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.modules.supplier_for_product.SupplierForProducts
import com.hrandika.seedling.spring.modules.address.Address
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "supplier")
data class Supplier(
    @Id
    var id: String? = null,

    @Column(nullable = false)
    var name: String? = null,

    var telephone: String? = null,

    var email: String? = null,


    var active: Boolean? = true,

    @OneToMany(fetch = FetchType.LAZY)
    var addresses : List<Address>? = null,

    @OneToMany(fetch = FetchType.LAZY)
    var supplierForProducts: List<SupplierForProducts>? = null

) : BaseEntity()

@Repository
interface SupplierRepository : PagingAndSortingRepository<Supplier, String>
