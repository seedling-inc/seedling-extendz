package com.hrandika.seedling.spring.modules.sales_charge_type

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import com.hrandika.seedling.spring.core.common.price.Price
import org.springframework.data.repository.PagingAndSortingRepository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "sales_charge_type")
data class SalesChargeType(
    @Id
    var id: String? = null,

    @Column(nullable = false)
    var name: String? = null,

    @Column(name= "default_amount", nullable = false)
    var defaultAmount: Price? = null,

    var active: Boolean? = true

) : BaseEntity()

interface SalesChargeTypeRepository : PagingAndSortingRepository<SalesChargeType, String>
