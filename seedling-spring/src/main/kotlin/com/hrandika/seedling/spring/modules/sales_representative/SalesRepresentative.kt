package com.hrandika.seedling.spring.modules.sales_representative

import com.hrandika.seedling.spring.core.common.base_entity.BaseEntity
import org.springframework.data.repository.PagingAndSortingRepository
import org.hibernate.annotations.Type
import javax.persistence.*

@Entity
@Table(name = "sales_representative")
data class SalesRepresentative(
    @Id
    var id: String? = null,

    @Column(name = "first_name", nullable = false)
    var firstName: String? = null,

    @Column(name = "last_name", nullable = false)
    var lastName: String? = null,

    var telephone: String? = null,

    var email: String? = null,

    var active: Boolean? = true

) : BaseEntity()

interface SalesRepresentativeRepository : PagingAndSortingRepository<SalesRepresentative, String>
