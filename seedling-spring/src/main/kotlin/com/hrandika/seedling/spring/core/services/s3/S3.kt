package com.hrandika.seedling.spring.core.services.s3

object S3Directories {
    const val BRAND_IMAGES = "brands/images/"
    const val CATEGORY_IMAGES = "category/images/"
    const val SUBCATEGORY_IMAGES = "sub_category/images/"
    const val PRODUCTS_IMAGES = "products/images/"
    const val BLOGPOSTS_IMAGES = "blogpost/images/"
    const val STOCK_KEEPING_UNIT_IMAGES = "stock_keeping_units/images/"
}