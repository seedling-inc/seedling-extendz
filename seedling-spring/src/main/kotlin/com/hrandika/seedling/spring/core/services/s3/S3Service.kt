package com.hrandika.seedling.spring.core.services.s3

import com.hrandika.seedling.spring.utils.logger
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider
import software.amazon.awssdk.core.sync.RequestBody
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.s3.S3Client
import software.amazon.awssdk.services.s3.model.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URI
import java.net.URL
import javax.annotation.PostConstruct

@Service
class S3Service {

    val log: Logger = logger<S3Service>()!!

    @Value("\${amazon.bucket-name}")
    private val bucketName: String? = null

    @Value("\${amazon.access-key}")
    private val accessKey: String? = null

    @Value("\${amazon.secret-key}")
    private val secretKey: String? = null

    @Value("\${amazon.region}")
    private val region: String? = null

    @Value("\${amazon.endpoint-url}")
    private val endPointUrl: String? = null

    private var s3: S3Client? = null;

//    @PostConstruct
    fun init() {
        val awsBasicCredentials = AwsBasicCredentials.create(this.accessKey, this.secretKey)
        val region: Region = Region.of(this.region)

        this.s3 = S3Client.builder()
            .region(region)
            .credentialsProvider(StaticCredentialsProvider.create(awsBasicCredentials))
            .endpointOverride(URI(this.endPointUrl))
            .build()

        tutorialSetup(this.s3, this.bucketName, region);

    }

    fun uploadImage(file: MultipartFile, parentPath: String): String {
        return try {
            val temporaryFile: File = this.convertMultiPartToFile(file)
            val url = uploadImageFile(temporaryFile, String.format("%s%s", parentPath, file.originalFilename))
            temporaryFile.delete()
            log.info("Temporary deleted {}", temporaryFile.absolutePath)
            url.toString()
        } catch (e: IOException) {
            e.printStackTrace()
            null!!
        }
    }

    @Throws(IOException::class)
    fun convertMultiPartToFile(file: MultipartFile): File {
        val temp = File.createTempFile(file.originalFilename, ".tmp")
        val fos = FileOutputStream(temp)
        fos.write(file.bytes)
        fos.close()
        log.info("Temporary file created {}", temp.absolutePath)
        return temp
    }

    fun uploadImageFile(file: File, key: String): URL? {
        log.info("uploading file {} ", key)
        val request = PutObjectRequest.builder()
            .bucket(this.bucketName)
            .key(key)
            .contentType("image/jpeg")
            .acl(ObjectCannedACL.PUBLIC_READ)
            .build()
        this.s3?.putObject(request, RequestBody.fromFile(file));
        val get = GetUrlRequest.builder().bucket(this.bucketName)
            .endpoint(URI(this.endPointUrl))
            .key(key).build()
        return s3?.utilities()?.getUrl(get)
    }

    fun tutorialSetup(s3Client: S3Client?, bucketName: String?, region: Region) {
        try {
            s3Client?.createBucket(
                CreateBucketRequest
                    .builder()
                    .bucket(bucketName)
                    .createBucketConfiguration(
                        CreateBucketConfiguration.builder()
                            .locationConstraint(region.id())
                            .build()
                    )
                    .build()
            )
            println("Creating bucket: $bucketName")
            s3Client?.waiter()?.waitUntilBucketExists(
                HeadBucketRequest.builder()
                    .bucket(bucketName)
                    .build()
            )
            println("$bucketName is ready.")
            System.out.printf("%n")
        } catch (e: S3Exception) {
            System.err.println(e.awsErrorDetails().errorMessage())
        }
    }

}